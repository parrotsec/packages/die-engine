#!/bin/bash -x
QT_PATH=/usr/lib/x86_64-linux-gnu
RELEASE_VERSION=$(cat "release_version.txt")
echo $RELEASE_VERSION
SOURCE_PATH=$PWD

BUILD_NAME=die_engine

cd $SOURCE_PATH

function makeproject
{
    cd $SOURCE_PATH/$1

    qmake $1.pro -spec linux-g++
    make -f Makefile clean
    make -f Makefile

    rm -rf Makefile
    rm -rf Makefile.Release
    rm -rf Makefile.Debug
    rm -rf object_script.*

    cd $SOURCE_PATH
}


makeproject build_libs
makeproject gui_source # sudo apt install qtscript5-dev

makeproject console_source

cd $SOURCE_PATH/gui_source
lupdate gui_source_tr.pro
cd $SOURCE_PATH

mkdir -p $SOURCE_PATH/release
rm -rf $SOURCE_PATH/release/$BUILD_NAME
mkdir -p $SOURCE_PATH/release/$BUILD_NAME
mkdir -p $SOURCE_PATH/release/$BUILD_NAME/base
mkdir -p $SOURCE_PATH/release/$BUILD_NAME/base/platforms

cp -R $SOURCE_PATH/build/release/die                     		$SOURCE_PATH/release/$BUILD_NAME/base/
cp -R $SOURCE_PATH/build/release/diec                     		$SOURCE_PATH/release/$BUILD_NAME/base/


mkdir -p $SOURCE_PATH/release/$BUILD_NAME/base/lang
cp -Rf $SOURCE_PATH/XStyles/qss/ $SOURCE_PATH/release/$BUILD_NAME/base/qss/
cp -Rf $SOURCE_PATH/Detect-It-Easy/info/ $SOURCE_PATH/release/$BUILD_NAME/base/info/
cp -Rf $SOURCE_PATH/Detect-It-Easy/db/ $SOURCE_PATH/release/$BUILD_NAME/base/db/

lrelease  $SOURCE_PATH/gui_source/translation/die_de.ts -qm  $SOURCE_PATH/release/$BUILD_NAME/base/lang/die_de.qm
lrelease  $SOURCE_PATH/gui_source/translation/die_ja.ts -qm  $SOURCE_PATH/release/$BUILD_NAME/base/lang/die_ja.qm
lrelease  $SOURCE_PATH/gui_source/translation/die_pl.ts -qm  $SOURCE_PATH/release/$BUILD_NAME/base/lang/die_pl.qm
lrelease  $SOURCE_PATH/gui_source/translation/die_pt_BR.ts -qm  $SOURCE_PATH/release/$BUILD_NAME/base/lang/die_pt_BR.qm
lrelease  $SOURCE_PATH/gui_source/translation/die_fr.ts -qm  $SOURCE_PATH/release/$BUILD_NAME/base/lang/die_fr.qm
lrelease  $SOURCE_PATH/gui_source/translation/die_ru.ts -qm  $SOURCE_PATH/release/$BUILD_NAME/base/lang/die_ru.qm
lrelease  $SOURCE_PATH/gui_source/translation/die_vi.ts -qm  $SOURCE_PATH/release/$BUILD_NAME/base/lang/die_vi.qm
lrelease  $SOURCE_PATH/gui_source/translation/die_zh.ts -qm  $SOURCE_PATH/release/$BUILD_NAME/base/lang/die_zh.qm
lrelease  $SOURCE_PATH/gui_source/translation/die_zh_TW.ts -qm  $SOURCE_PATH/release/$BUILD_NAME/base/lang/die_zh_TW.qm
lrelease  $SOURCE_PATH/gui_source/translation/die_tr.ts -qm  $SOURCE_PATH/release/$BUILD_NAME/base/lang/die_tr.qm

